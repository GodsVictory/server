module gitlab.com/controllercubiomes/server

go 1.14

require (
	github.com/gin-gonic/gin v1.6.2
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.4.0
	gitlab.com/controllercubiomes/util v0.1.4-beta
)
